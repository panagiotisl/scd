package scd;

import static org.junit.Assert.*;
import gr.uoa.di.madgik.scd.Community;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.clearspring.analytics.stream.frequency.CountMinSketch;

public class CommunityTest {

	@Test
	public void isSeedTest(){
		Set<String> set = new HashSet<String>();
		set.add("1");
		Community comm = new Community(set);
		assertEquals(comm.isSeed("1"), true);
		assertEquals(comm.isSeed("2"), false);
	}
	
	@Test
	public void isMemberTest(){
		Set<String> set = new HashSet<String>();
		set.add("1");
		Community comm = new Community(set);
		comm.add("2");
		assertEquals(comm.contains("1"), true);
		assertEquals(comm.contains("2"), true);
		assertEquals(comm.contains("3"), false);
	}
	
	@Test
	public void canPruneCommunity(){
		CountMinSketch commCMS;
		CountMinSketch degreeCMS;
		double epsOfTotalCount = 0.0001;
	    double confidence = 0.99;
	    commCMS = new CountMinSketch(epsOfTotalCount, confidence, 23);
	    degreeCMS = new CountMinSketch(epsOfTotalCount, confidence, 23);
	    Set<String> set = new HashSet<String>();
		set.add("1");
		Community comm = new Community(set);
		for(int i=0;i< 200;i++){
			commCMS.add(Integer.toString(i), (long) (i*0.7+1));
			degreeCMS.add(Integer.toString(i), 200);
			comm.add(Integer.toString(i));
		}
		comm.pruneCommunity(commCMS, degreeCMS, 100);
		assertEquals(comm.size(), 100);
	}
	
}
