package gr.uoa.di.madgik.scd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.clearspring.analytics.stream.frequency.CountMinSketch;

public class SCDBolt extends BaseBasicBolt {

	private static final Logger LOGGER = Logger.getLogger(BaseBasicBolt.class); 
	
	private static final long serialVersionUID = 7269162899119690223L;
	private static final String DELIMITER = " ";
	
	private int elementsProcessed;
	private CountMinSketch commCMS;
	private CountMinSketch degreeCMS;
	private List<Community> communities;
	
	
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		elementsProcessed++;
		String nodes[] = tuple.getString(0).split(DELIMITER);
		addToCommCMS(nodes);
		addToDegreeCMS(nodes);
		if(elementsProcessed % 10000 == 0){
			collector.emit(new Values(nodes[0], commCMS.estimateCount(nodes[0])));
			LOGGER.info("Another 10,000..." + " Community size: " + communities.get(0).size());
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("node", "count"));
	}

	public void prepare(Map stormConf, TopologyContext context) {
		elementsProcessed = 0;
		double epsOfTotalCount = 0.0001;
	    double confidence = 0.99;
	    commCMS = new CountMinSketch(epsOfTotalCount, confidence, 23);
	    degreeCMS = new CountMinSketch(epsOfTotalCount, confidence, 23);
	    communities = new ArrayList<Community>();
	    Set<String> set = new HashSet<String>();
	    set.add("1");
	    communities.add(new Community(set));
	}
	
	private void addToCommCMS(String[] nodes){
		
		for(int i=0;i<communities.size();i++){
			Community comm = communities.get(i);
			// if adjacent node is a seed, add 1 
			if(comm.isSeed(nodes[0])){
				commCMS.add(nodes[1], 1);
			}
			// else if adjacent node is a member add estimate of participation / estimate of degree
			else if (comm.contains(nodes[0])){
				commCMS.add(nodes[1], commCMS.estimateCount(i+":"+nodes[0])/degreeCMS.estimateCount(nodes[0]));
			}
			if(comm.isSeed(nodes[1])){
				commCMS.add(nodes[0], 1);
			}
			else if (comm.contains(nodes[1])){
				commCMS.add(nodes[0], commCMS.estimateCount(i+":"+nodes[1])/degreeCMS.estimateCount(nodes[1]));
			}
			// if adjacent node is a member add node to community
			if (comm.contains(nodes[0])){
				comm.add(nodes[1]);
			}
			if (comm.contains(nodes[1])){
				comm.add(nodes[0]);
			}
		}
	}
	
	private void addToDegreeCMS(String[] nodes){
		degreeCMS.add(nodes[0], 1);
		degreeCMS.add(nodes[1], 1);
	}
	
}
