package gr.uoa.di.madgik.scd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.analysis.ParametricUnivariateFunction;
import org.apache.commons.math3.optimization.fitting.CurveFitter;
import org.apache.commons.math3.optimization.general.LevenbergMarquardtOptimizer;
import org.apache.log4j.Logger;
import org.apache.storm.shade.com.google.common.io.Resources;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

public class StreamCD {

	private static final String DELIMITER = " ";
	private static final int MAX_COMMUNITY_SIZE = 100;
	private static final Random rng = new Random(23); // Ideally just create one instance globally
	private static final Logger LOGGER = Logger.getLogger(StreamCD.class);
	private static final int NUMBER_OF_SEEDS = 3;

	public static void main(String[] args) throws IOException {

		File result = new File("temp.txt");
		BufferedWriter bw = new BufferedWriter(new FileWriter(result));
		
		File gtcFile = new File(Resources.getResource("amazonGTC.txt").getFile());
		BufferedReader gtcFileBR = new BufferedReader(new FileReader(gtcFile));
		
		String readLine;

		double epsOfTotalCount = 0.0001;
		double confidence = 0.99;
		List<Community> communities = new ArrayList<Community>();
		String commLine;
		while((commLine = gtcFileBR.readLine()) != null){
			String[] comm = commLine.trim().split(DELIMITER);
			Set<Integer> randomNumbers = getRandomNumbers(comm.length, NUMBER_OF_SEEDS);
			
			HashSet<String> set  = new HashSet<String>();
			for(int number : randomNumbers){
				set.add(comm[number]);
			}
		    communities.add(new Community(set, comm));
		}
		File graphFile = new File(Resources.getResource("amazon1.txt").getFile());
		BufferedReader graphFileBR = new BufferedReader(new FileReader(graphFile));
		
		int elementsProcessed = 0;
//		SimpleGraph<String, DefaultEdge> graph = new SimpleGraph<>(
//				DefaultEdge.class);
		DoubleCountMinSketch commCMS = new DoubleCountMinSketch(
				epsOfTotalCount, confidence, 23);
		DoubleCountMinSketch degreeCMS = new DoubleCountMinSketch(
				epsOfTotalCount, confidence, 23);
		
		while ((readLine = graphFileBR.readLine()) != null) {
			

			elementsProcessed++;
			String nodes[] = readLine.split(DELIMITER);
			// do not allow self loops
			if (nodes[0].equals(nodes[1]))
				continue;
//			addToGraph(nodes, graph);
			addToDegreeCMS(nodes, degreeCMS);
			addToCommCMS(nodes, commCMS, degreeCMS, communities);
			if (elementsProcessed % 10000 == 0) {
				communities.forEach((community) -> {
					community.pruneCommunity(commCMS, degreeCMS, MAX_COMMUNITY_SIZE);
				});
			}
		}
		graphFileBR.close();
		communities.forEach((community) -> {
//			writeCommunityByConductanceOptimization(community, graph, bw, commCMS, degreeCMS, comm);
//			writeCommunityByThreshold(community, graph, bw, commCMS, degreeCMS, comm);
//			writeCommunityByPowerLawThreshold(community, graph, bw, commCMS, degreeCMS, comm);
			writeCommunityByRegressionTree(community, bw, commCMS, degreeCMS);
		});
		
		bw.close();
		gtcFileBR.close();
	}

	
	private static void writeCommunityByConductanceOptimization(Community community, SimpleGraph graph, BufferedWriter bw, DoubleCountMinSketch commCMS, DoubleCountMinSketch degreeCMS, String[] comm){
		int bestSize = 0;
		double bestConductance = Double.MAX_VALUE;
		community.getSortedCommunity();
		for (int size = 2; size <= MAX_COMMUNITY_SIZE; size += 1) {
			double tempConductance = community.getConductance(graph, size);
			if(tempConductance < bestConductance){
				bestConductance = tempConductance;
				bestSize = size;
			}
		}
		LOGGER.info(bestConductance + "\t" + bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue());
		System.out.println("K " + bestSize + " " + community.getSortedCommunity().get(bestSize-1).getValue() + " " + community.getSortedCommunity().stream().map(entry -> entry.getValue()).collect(Collectors.toCollection(LinkedList::new)));
		try {
			bw.write(bestConductance + "\t" + bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue() + "\t" + getF1Score(community.getPrunedCommunity(commCMS, degreeCMS, bestSize).keyset(), comm) + "\t");
			bw.flush();
		} catch (Exception e) {
			LOGGER.error("ERROR", e);
		}
	}
	
	private static void writeCommunityByThreshold(Community community, SimpleGraph graph, BufferedWriter bw, DoubleCountMinSketch commCMS, DoubleCountMinSketch degreeCMS, String[] comm){
		int bestSize = 0;
		for(Entry<String, Double> entry : community.getSortedCommunity()){
			if(entry.getValue() > 0.0001){
				bestSize++;
			}
			else{
				break;
			}
		}
		LOGGER.info(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue());
		try {
			bw.write(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue() + "\t" + getF1Score(community.getPrunedCommunity(commCMS, degreeCMS, bestSize).keyset(), comm) + "\t" + community.getMeanValue() + "\t" + community.getMedianValue() + "\t" + community.getVarianceValue() + "\t" + community.getSortedCommunity().get(community.size() - 1).getValue() + "\t");
			bw.flush();
		} catch (Exception e) {
			System.out.println("ERROR");
		}
	}
	
	private static void writeCommunityByPowerLawThreshold(Community community, SimpleGraph graph, BufferedWriter bw, DoubleCountMinSketch commCMS, DoubleCountMinSketch degreeCMS, String[] comm){
		
		community.getSortedCommunity().forEach( ( x -> { System.out.println(x.getValue()); } ));
		
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		CurveFitter<ParametricUnivariateFunction> curveFitter = new CurveFitter(optimizer);
		int rank = 1;
		for(Entry<String, Double> entry : community.getSortedCommunity()){
			if(rank >= 10 || community.size() < 10)
				curveFitter.addObservedPoint( rank,  entry.getValue());
			rank++;
		}
		double a = fitPowerLawCurve(curveFitter);
		int bestSize = 0;
//		double threshold = 0.06174582325778034 * a - 0.005248009737927453;
//		double threshold = 0.030312789258194056 * a -0.002518659244016383;
		double threshold = 0.44400709230716906 * a - 0.0009328724508026326;
		for(Entry<String, Double> entry : community.getSortedCommunity()){
			if(entry.getValue() > threshold) {
				bestSize++;
			}
			else{
				break;
			}
		}
		LOGGER.info(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue());
		try {
			bw.write(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue() + "\t" + getF1Score(community.getPrunedCommunity(commCMS, degreeCMS, bestSize).keyset(), comm) + "\t" + community.getMeanValue() + "\t" + community.getMedianValue() + "\t" + community.getVarianceValue() + "\t" + community.getSortedCommunity().get(community.size() - 1).getValue() + "\t" + a + "\t");
			bw.flush();
		} catch (Exception e) {
			System.out.println("ERROR");
		}
	}
	
	private static void writeCommunityByRegressionTree(Community community, BufferedWriter bw, DoubleCountMinSketch commCMS, DoubleCountMinSketch degreeCMS){
		int bestSize = 0;
		double threshold = 0;
		List<Entry<String, Double>> sortedCommunity = community.getSortedCommunity();
//		if (sortedCommunity.size()>94 && sortedCommunity.get(94).getValue() > 3.12951e-05) {
//			if (sortedCommunity.get(7).getValue() > 0.03676613) {
//				threshold = 0.017917790;
//			} else {
//				threshold = 0.009550473;
//			}
//		} else {
//			threshold = 0.003305544;
//		}
		
//		 1) root 1219 5.890602e-02 0.0034502450  
//		    2) X29< 0.02020892 1155 3.731570e-02 0.0025994540  
//		      4) X85< 7.52093e-05 1025 1.910376e-02 0.0015924270  
//		        8) X8< 0.03843032 965 1.387081e-02 0.0012846520  
//		         16) X15>=0.0004154366 944 9.288028e-03 0.0010976770  
//		           32) X6< 0.03567974 776 4.559582e-03 0.0007654801 *
//		           33) X6>=0.03567974 168 4.247257e-03 0.0026321090  
//		             66) X85< 1.418593e-06 147 2.738291e-03 0.0020491800 *
//		             67) X85>=1.418593e-06 21 1.109355e-03 0.0067126120  
//		              134) X44>=0.0003786994 13 2.239667e-05 0.0013731950 *
//		              135) X44< 0.0003786994 8 1.140757e-04 0.0153891600 *
//		         17) X15< 0.0004154366 21 3.066261e-03 0.0096896420  
//		           34) X4< 0.03282142 11 3.659395e-04 0.0019868780 *
//		           35) X4>=0.03282142 10 1.329739e-03 0.0181626800 *
//		        9) X8>=0.03843032 60 3.671364e-03 0.0065424750 *
//		      5) X85>=7.52093e-05 130 8.976775e-03 0.0105394700  
//		       10) X67>=0.0003217508 111 6.958116e-03 0.0092827790  
//		         20) X7< 0.03299951 63 3.145074e-03 0.0065336440  
//		           40) X2< 0.04361161 13 7.926355e-06 0.0008253990 *
//		           41) X2>=0.04361161 50 2.603420e-03 0.0080177880  
//		             82) X8>=0.02770231 21 5.929559e-04 0.0036839360 *
//		             83) X8< 0.02770231 29 1.330417e-03 0.0111560900 *
//		         21) X7>=0.03299951 48 2.711973e-03 0.0128910200 *
//		       11) X67< 0.0003217508 19 8.192404e-04 0.0178812000 *
//		    3) X29>=0.02020892 64 5.666324e-03 0.0188043700  
//		      6) X16< 0.04462325 48 1.654879e-03 0.0155441000 *
//		      7) X16>=0.04462325 16 1.970607e-03 0.0285851900 *
		
//		if (sortedCommunity.size() < 29 || sortedCommunity.get(28).getValue() < 0.02020892) {
//			if (sortedCommunity.size() < 85 || sortedCommunity.get(84).getValue() < 7.52093e-05) {
//				if (sortedCommunity.size() < 8 || sortedCommunity.get(7).getValue() < 0.03843032) {
//					if (sortedCommunity.size() >= 15 && sortedCommunity.get(14).getValue() >= 0.0004154366) {
//						if (sortedCommunity.size() < 6 || sortedCommunity.get(5).getValue() < 0.03567974) {
//							threshold = 0.0007654801;
//						} else {
//							if (sortedCommunity.size() < 85 || sortedCommunity.get(84).getValue() <  1.418593e-06) {
//								threshold = 0.0020491800;
//							}
//							else {
//								if (sortedCommunity.size() >= 44 && sortedCommunity.get(43).getValue() >=  0.00037869946) {
//									threshold = 0.0013731950;
//								} else {
//									threshold = 0.0153891600;
//								}
//							}
//						}
//					} else {
//						if (sortedCommunity.size() < 4 || sortedCommunity.get(3).getValue() <  0.03282142) {
//							threshold = 0.0019868780;
//						} else {
//						threshold = 0.0181626800;
//						}
//					}
//						
//				} else {
//					threshold = 0.0065424750;
//				}
//			} else {
//				if (sortedCommunity.size() >= 67 && sortedCommunity.get(66).getValue() >=  0.0003217508) {
//					if (sortedCommunity.size() < 7 || sortedCommunity.get(6).getValue() <  0.03299951) {
//						if (sortedCommunity.size() < 2 || sortedCommunity.get(1).getValue() <  0.04361161) {
//							threshold = 0.0008253990;
//						} else {
//							if (sortedCommunity.size() >= 8 && sortedCommunity.get(7).getValue() >=  0.02770231) {
//								threshold = 0.0036839360;
//							} else {
//								threshold = 0.0111560900;
//							}
//						}
//					} else {
//						threshold = 0.0128910200;
//					}
//				} else {
//					threshold = 0.0178812000;
//				}
//			}
//		} else {
//			if (sortedCommunity.size() < 16 || sortedCommunity.get(15).getValue() <  0.04462325) {
//				threshold = 0.0155441000;
//			} else {
//				threshold = 0.0285851900;
//			}
//		}
		
		
		
//		 1) root 427 0.0374663100 0.0077101800  
//		   2) X95< 3.12951e-05 229 0.0116886100 0.0033055440  
//		     4) X6< 0.0572146 222 0.0072263790 0.0027337110  
//		       8) X5< 0.03237804 111 0.0006994963 0.0008738819 *
//		       9) X5>=0.03237804 111 0.0057589930 0.0045935390 *
//		     5) X6>=0.0572146 7 0.0020874220 0.0214408300 *
//		   3) X95>=3.12951e-05 198 0.0161965400 0.0128044300  
//		     6) X8< 0.03676613 121 0.0076025470 0.0095504730  
//		      12) X27>=0.001326233 106 0.0054230370 0.0084265600 *
//		      13) X27< 0.001326233 15 0.0010994070 0.0174927900 *
//		     7) X8>=0.03676613 77 0.0052995360 0.0179177900  
//		      14) X15< 0.04761905 66 0.0029791480 0.0163906000 *
//		      15) X15>=0.04761905 11 0.0012428560 0.0270809500 *
		
		if (sortedCommunity.size() < 95 || sortedCommunity.get(94).getValue() < 3.12951e-05) {
			if (sortedCommunity.size() < 6 || sortedCommunity.get(5).getValue() < 0.0572146) {
				if (sortedCommunity.size() < 5 || sortedCommunity.get(4).getValue() < 0.03237804) {
					threshold = 0.0008738819;
				} else {
					threshold = 0.0045935390;
				} 
			} else {
				threshold = 0.0214408300;
			}
		} else {
			if (sortedCommunity.size() < 8 || sortedCommunity.get(7).getValue() < 0.03676613) {
				if (sortedCommunity.size() >= 27 && sortedCommunity.get(26).getValue() >= 0.001326233) {
					threshold = 0.0084265600;
				} else {
					threshold = 0.0174927900;
				}
			} else {
				if (sortedCommunity.size() < 15 || sortedCommunity.get(14).getValue() < 0.04761905) {
					threshold = 0.0163906000;
				} else {
					threshold = 0.0270809500;
				}
			}
		}
		
		
		
		for(Entry<String, Double> entry : sortedCommunity){
			if(entry.getValue() > threshold){
				bestSize++;
			}
			else{
				break;
			}
		}
		LOGGER.info(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue());
		try {
			bw.write(bestSize + "\t" + community.getSortedCommunity().get(bestSize-1).getValue() + "\t" + getF1Score(community.getPrunedCommunity(commCMS, degreeCMS, bestSize).keyset(), community.getGroundTruth()) + "\t" + community.getMeanValue() + "\t" + community.getMedianValue() + "\t" + community.getVarianceValue() + "\t" + community.getSortedCommunity().get(community.size() - 1).getValue() + "\t");
			bw.write('\n');
			bw.flush();
		} catch (Exception e) {
			System.out.println("ERROR");
		}
	}

	
	private static Set<Integer> getRandomNumbers(int max, int numbersNeeded) {
		if (max < numbersNeeded)
		{
		    throw new IllegalArgumentException("Can't ask for more numbers than are available");
		}
		// Note: use LinkedHashSet to maintain insertion order
		Set<Integer> generated = new LinkedHashSet<Integer>();
		while (generated.size() < numbersNeeded)
		{
		    Integer next = rng.nextInt(max);
		    // As we're adding to a set, this will automatically do a containment check
		    generated.add(next);
		}
		return generated;
	}

	private static void addToCommCMS(String[] nodes,
			DoubleCountMinSketch commCMS, DoubleCountMinSketch degreeCMS,
			List<Community> communities) {

		for (int i = 0; i < communities.size(); i++) {
			Community comm = communities.get(i);
			// if adjacent node is a seed, add 1
			if (comm.isSeed(nodes[0])) {
				commCMS.add(i + ":" + nodes[1], 1);
			}
			// else if adjacent node is a member add estimate of participation /
			// estimate of degree
			else if (comm.contains(nodes[0])) {
				commCMS.add(i + ":" + nodes[1], comm.get(nodes[0]));
//				commCMS.add(i + ":" + nodes[1], 1);
			}
			// if adjacent node is a seed, add 1
			if (comm.isSeed(nodes[1])) {
				commCMS.add(i + ":" + nodes[0], 1);
			}
			// else if adjacent node is a member add estimate of participation /
			// estimate of degree				
			else if (comm.contains(nodes[1])) {
				commCMS.add(i + ":" + nodes[0], comm.get(nodes[1]));
//				commCMS.add(i + ":" + nodes[0], 1);
			}
			// if adjacent node is a member add node to community
			if (comm.contains(nodes[0])) {
				comm.put(nodes[1],
						1 - ((degreeCMS.estimateCount(nodes[1]) - commCMS
								.estimateCount(i + ":" + nodes[1])) / degreeCMS
								.estimateCount(nodes[1])));
			}
			if (comm.contains(nodes[1])) {
				comm.put(nodes[0],
						1 - ((degreeCMS.estimateCount(nodes[0]) - commCMS
								.estimateCount(i + ":" + nodes[0])) / degreeCMS
								.estimateCount(nodes[0])));
			}
		}
	}

	private static void addToDegreeCMS(String[] nodes,
			DoubleCountMinSketch degreeCMS) {
		degreeCMS.add(nodes[0], 1);
		degreeCMS.add(nodes[1], 1);
	}

	private static void addToGraph(String[] nodes,
			SimpleGraph<String, DefaultEdge> graph) {
		if (!graph.containsVertex(nodes[0])) {
			graph.addVertex(nodes[0]);
		}
		if (!graph.containsVertex(nodes[1])) {
			graph.addVertex(nodes[1]);
		}
		if (!graph.containsEdge(nodes[0], nodes[1])) {
			graph.addEdge(nodes[0], nodes[1]);
		}
	}
	
	private static double getPrecision(Set<String> found, Set<String> gtc, SetView<String> common){
		return (double)common.size()/found.size();
	}
	
	private static double getRecall(Set<String> found, Set<String> gtc, SetView<String> common){
		return (double)common.size()/gtc.size();
	}
	
	private static double getF1Score(Set<String> found, String[] comm){
		HashSet<String> gtc = new HashSet<String>(Arrays.asList(comm));
		SetView<String> common = Sets.intersection(found, gtc);
		double precision = getPrecision(found, gtc, common);
		double recall = getRecall(found, gtc, common);
		if(precision == 0 && recall == 0)
			return 0;
		else
			return 2 * ( precision * recall ) / ( precision + recall );
	}
	
	private static double fitPowerLawCurve(CurveFitter<ParametricUnivariateFunction> curveFitter) {

		ParametricUnivariateFunction f = new ParametricUnivariateFunction() {
            public double value(double x, double ... parameters) {
                double a = parameters[0];
//                return a * Math.pow(x, a - 1);
                return a * Math.pow(x, -a);
            }

            public double[] gradient(double x, double ... parameters) {
                double a = parameters[0];
                
                double[] gradients = new double[1];

                // derivative with respect to a
//                gradients[0] = Math.pow(x, a - 1) * (Math.log(x) * a + 1);
                gradients[0] = (1 - Math.log(x)*a) / Math.pow(x, a); 
                
                return gradients;

            }
        };
        
        double[] initialGuess = new double[] { 0.1};
        double[] estimatedParameters = curveFitter.fit(f, initialGuess);
        double a = estimatedParameters[0];
        
        return a;
	}

}
