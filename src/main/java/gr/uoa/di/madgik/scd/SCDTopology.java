package gr.uoa.di.madgik.scd;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

public class SCDTopology {

	public static void main(String[] args){
		
		TopologyBuilder tb = new TopologyBuilder();
		
		tb.setSpout("graphSpout", new SCDSpout(), 1);
		
		tb.setBolt("scdBolt", new SCDBolt()).globalGrouping("graphSpout");
		
		Config conf = new Config();
		
//		conf.setDebug(true);
		
		conf.setMaxTaskParallelism(1);
		
		LocalCluster cluster = new LocalCluster();
		
		cluster.submitTopology("GraphTopology", conf, tb.createTopology());
	}
	
}
