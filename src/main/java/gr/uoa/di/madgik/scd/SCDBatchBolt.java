package gr.uoa.di.madgik.scd;

import java.util.Map;

import org.apache.storm.coordination.BatchOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBatchBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.clearspring.analytics.stream.frequency.CountMinSketch;

public class SCDBatchBolt extends BaseBatchBolt implements IRichBolt {

	private static final long serialVersionUID = 9167747620600349160L;
	private static final String DELIMITER = " ";
	
    private CountMinSketch commCMS;
	
    Object _id;
    BatchOutputCollector _collector;

	public void execute(Tuple tuple) {
		String nodes[] = tuple.getString(0).split(DELIMITER);
		commCMS.add(nodes[0], 1);
		commCMS.add(nodes[1], 1);
	}

	public void finishBatch() {
		_collector.emit(new Values(commCMS.estimateCount("0")));
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("count"));
	}

	public void prepare(Map conf, TopologyContext context,
			BatchOutputCollector collector, Object id) {
		   _collector = collector;
		   _id = id;
			double epsOfTotalCount = 0.0001;
		    double confidence = 0.99;
		    commCMS = new CountMinSketch(epsOfTotalCount, confidence, 23);
		
	}

	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		
	}

	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

}
