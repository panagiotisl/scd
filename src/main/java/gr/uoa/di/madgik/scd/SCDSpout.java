package gr.uoa.di.madgik.scd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

import org.apache.storm.shade.com.google.common.io.Resources;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

public class SCDSpout extends BaseRichSpout {

	private static final long serialVersionUID = 8305333229062573150L;

	SpoutOutputCollector _collector;
	
	private BufferedReader br;
	private Random random = new Random(23);
	
	public void nextTuple() {
//		Utils.sleep(random.nextInt(20));
		String readLine;
		try {
			if ((readLine = br.readLine()) != null) {
				_collector.emit(new Values(readLine));
			}
		} catch (IOException e) { 
		}
	}

	public void open(Map map, TopologyContext tc, SpoutOutputCollector collector) {
		_collector = collector;
		 try {
	            File f = new File(Resources.getResource("citation-csconf-mult5-PaperJSON.graph.txt").getFile());
	            br = new BufferedReader(new FileReader(f));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("edge"));
	}

}
